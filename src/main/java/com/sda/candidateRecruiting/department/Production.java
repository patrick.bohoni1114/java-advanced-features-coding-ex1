package com.sda.candidateRecruiting.department;

import com.sda.candidateRecruiting.candidate.Candidate;
import com.sda.candidateRecruiting.candidate.CandidateStatus;

public class Production extends Department {

    public Production() {
        this.name = DepartmentName.PRODUCTION;
        this.minLevelOfCompetence = 5;
    }


    public void evaluate(Candidate candidate) {
        if (candidate.getLevelOfCompetence() > this.minLevelOfCompetence){
            candidate.setCandidateStatus(CandidateStatus.ACCEPTED);
        } else {
            candidate.setCandidateStatus(CandidateStatus.REJECTED);
        }

    }
}
