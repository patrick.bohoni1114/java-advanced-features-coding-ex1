package com.sda.candidateRecruiting.department;

public enum DepartmentName {

    MARKETING,
    PRODUCTION,
    HR,
    IT

}
