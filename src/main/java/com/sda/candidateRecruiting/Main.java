package com.sda.candidateRecruiting;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.candidateRecruiting.candidate.Candidate;
import com.sda.candidateRecruiting.department.Department;
import com.sda.candidateRecruiting.department.DepartmentName;
import com.sda.candidateRecruiting.department.Marketing;
import com.sda.candidateRecruiting.department.Production;
import com.sda.candidateRecruiting.recruitment.Company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {


    public static void main(String[] args) throws IOException {
        List<Candidate> candidates = readCandidatesToJsonFile();



        Company company = new Company("SDA Recruiting", candidates);
        company.recruiting();

        wrieCandidatesToTxtFile(candidates);
        writeCandidatesToJsonFile(candidates);

    }

    public static void wrieCandidatesToTxtFile(List<Candidate> candidates) {
        try(
                BufferedWriter writer = new BufferedWriter(new FileWriter("candidati-acceptati.txt"))) {
            writer.write(candidates.toString()
            );
        } catch (IOException e) {
            System.out.println("Could not write to file.");
        }
    }

    public static void writeCandidatesToJsonFile(List<Candidate> candidates) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.writeValue(new File("candidati-acceptati.json"), candidates);



    }

    public static List<Candidate> readCandidatesToJsonFile() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Candidate> candidates = objectMapper.readValue(new File("candidati-initiali.json"), new TypeReference<List<Candidate>>(){});

        return candidates;




    }


}
