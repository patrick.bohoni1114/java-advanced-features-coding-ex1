package com.sda.candidateRecruiting.candidate;


public enum CandidateStatus {

    AWAITING, REJECTED, ACCEPTED

}
