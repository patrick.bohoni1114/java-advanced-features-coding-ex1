package com.sda.candidateRecruiting.candidate;

import com.sda.candidateRecruiting.department.Department;
import com.sda.candidateRecruiting.department.DepartmentName;

public class Candidate {

    private String name;
    private Integer age;
    private String address;
    private String emailAddress;
    private int levelOfCompetence;
    private int yearsOfExperience;
    private DepartmentName departmentName;
    private CandidateStatus candidateStatus;

    public Candidate(String name,
                     Integer age,
                     String address,
                     String emailAddress,
                     int levelOfCompetence,
                     int yearsOfExperience,
                     DepartmentName departmentName) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.emailAddress = emailAddress;
        this.levelOfCompetence = levelOfCompetence;
        this.yearsOfExperience = yearsOfExperience;
        this.departmentName = departmentName;
        this.candidateStatus = CandidateStatus.AWAITING;

    }

    public Candidate(){

    }



    public String getName() {
        return name;
    }

    public void setName(String numeleCandidatului) {
        this.name = numeleCandidatului;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setLevelOfCompetence(int levelOfCompetence) {
        this.levelOfCompetence = levelOfCompetence;
    }

    public int getLevelOfCompetence() {
        return this.levelOfCompetence;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public int getYearsOfExperience() {
        return this.yearsOfExperience;
    }

    public void setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
    }

    public DepartmentName getDepartmentName() {
        return this.departmentName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String toString() {
        return "Candidate: " + name + " has " + yearsOfExperience + " years of experience,  level of competence " +
                levelOfCompetence + "  applied for " + departmentName + " and has the status " +
                candidateStatus + "\n";
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
