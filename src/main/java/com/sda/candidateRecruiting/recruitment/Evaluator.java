package com.sda.candidateRecruiting.recruitment;

import com.sda.candidateRecruiting.candidate.Candidate;

public interface Evaluator {

    void evaluate(Candidate candidate);

}
