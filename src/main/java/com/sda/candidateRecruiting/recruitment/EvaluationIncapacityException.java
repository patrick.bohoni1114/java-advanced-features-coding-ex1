package com.sda.candidateRecruiting.recruitment;

/**
  checked exceptions = must be declared(using throws) or treated
  using unchecked exception = RuntimeExceptions
 */

public class EvaluationIncapacityException extends RuntimeException {

    public EvaluationIncapacityException() {
        super("This candidate did not apply for a valid department.");
    }

}
